#!/usr/bin/python3
import serial
import sys
import glob
import numpy as np
import time

import lib.scorbot

class NucleoConnection:
    """
    Controls a serial port communication with Scorbot-ER V nucleo.
    """
    def __init__(self, port="/dev/ttyACM1"):
        print("Instantiating NucleoConnection object..")
        ports = self.serialPorts()
        print(f"Ports found: {ports}")
        self.port = ports[0]
        self.s = self.openConnection(self.port)

    def serialPorts(self):
        """ Lists serial port names

            :raises EnvironmentError:
                On unsupported or unknown platforms
            :returns:
                A list of the serial ports available on the system
        """
        if sys.platform.startswith('win'):
            ports = ['COM%s' % (i + 1) for i in range(256)]
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            ports = glob.glob('/dev/tty[A-Za-z]*')
        else:
            raise EnvironmentError('Unsupported platform')

        result = []
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                result.append(port)
            except (OSError, serial.SerialException):
                pass
        return result

    def atMove(self, thetas=np.zeros(5, dtype=int)):
        """Moves robot to given joints configuration."""
        ec = self.radsToEncs(thetas)
        cmd = f'AT+MOVE {ec[0]},{ec[1]},{ec[2]},{ec[3]},{ec[4]}'
        return self.sendCommand(cmd)

    def atVelo(self, speed=50):
        """Sets robot's speed to the given percentage."""
        speed = round(speed)
        if speed in range(101):
            cmd = f'AT+VELO {speed}'
            self.sendCommand(cmd)
            return 0
        else:
            print("error: expected number within [0,100]")
            return -1

    def atOpen(self):
        """Opens the grip."""
        cmd = f'AT+OPEN'
        self.sendCommand(cmd)
        return 0

    def atClse(self):
        """Closes the grip."""
        cmd = f'AT+CLSE'
        self.sendCommand(cmd)
        return 0

    def atStop(self):
        """Stops the robot cancelling every given order."""
        cmd = f'AT+STOP'
        self.sendCommand(cmd)
        return 0

    def atStrt(self):
        """Starts the robot???."""
        cmd = f'AT+STRT'
        self.sendCommand(cmd)
        return 0

    def atEnco(self):
        """Asks for the encoder values. Returns decoded response."""
        cmd = f'AT+ENCO'
        self.sendCommand(cmd)
        return 0
        # return self.s.readline().decode("UTF-8")

    def atSave(self, num):
        """Saves the current joints values to the given number."""
        cmd = f'AT+SAVE {num}'
        self.sendCommand(cmd)
        return 0

    def atLoad(self, num):
        """Loads some previously saved joint values configuration on the given number."""
        cmd = f'AT+LOAD {num}'
        self.sendCommand(cmd)
        return 0

    def atCntl(self):
        """Switch robot control between programm/joystick."""
        cmd = f'AT+CNTL'
        self.sendCommand(cmd)
        return 0

    def atHome(self):
        """Sends the robot an order to go to home position."""
        cmd = f'AT+HOME'
        self.sendCommand(cmd)
        return 0

    def sendCommand(self, cmd):
        """Sends built command to serial port and waits for answer. Returns bytes written."""
        print(cmd)
        cmd = f'{cmd}\n'
        written = self.s.write(cmd.encode())
        return cmd.encode()

    def radsToEncs(self, thetas):
        """Returns a numpy.array with the converted thetas values."""
        ec = np.array([145, 142, 115, 109, 117], dtype=int)*np.array(thetas*180/np.pi, dtype=int)
        ec[3], ec[4] = [ec[3]+ec[4], ec[3]-ec[4]]  # Differential motors
        return ec

    def encsToRads(self, encs):
        """Returns a numpy.array with the converted encoders values."""
        encs[3], encs[4] = [(encs[3]+encs[4])/2, (encs[3]-encs[4])/2]  # Differential motors
        thetas =  np.array(encs)*np.pi/180/np.array([145, 142, 115, 109, 117])
        return thetas


    def openConnection(self, port):
        print(f'Atempting to connect on port {port}...')
        try:
            self.s = serial.Serial(port=port, timeout=2, baudrate=115200)
        except Exception as err:
            print(err)
            return -1
        return self.s

    def executeProgram(self):
        cmdList = [
            "AT+OPEN\n",
            "AT+VELO 50\n",
            "AT+CLSE\n",
            "AT+LOAD 0\n",
            "AT+LOAD 1\n",
            "AT+OPEN\n",
            "AT+VELO 25\n",
            "AT+LOAD 2\n",
            "AT+CLSE\n",
            "AT+VELO 45\n",
            "AT+LOAD 1\n",
            "AT+VELO 100\n",
            "AT+LOAD 3\n",
            "AT+VELO 25\n",
            "AT+LOAD 4\n",
            "AT+OPEN\n",
            "AT+VELO 45\n",
            "AT+LOAD 3\n",
            "AT+CLSE\n",
            "AT+LOAD 0\n"
        ]
        for i in range(len(cmdList)):
            print(cmdList[i])
            # s.write(cmdList[i].encode())
            encoders = s.readline()
            print(encoders)
            input("Press Enter to continue...")

if __name__ == "__main__":
    nc = NucleoConnection()
    print(nc.encsToRads([3418, 8009, -6709, -1512, -1501]))

    # b'0 0 0 0 0\r\n'