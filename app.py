#! /usr/bin/python3

""" App module to pack scorbot behaviour together. """

import sys
from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication, QDialog
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import numpy as np
import random
import time

import gui.mainWindow, gui.portSelection, gui.fileToSave
import lib.scorbot as scorbot
import lib.com as nucleo
from lib.scorbot import IK

# Classes
class App(QtWidgets.QMainWindow, gui.mainWindow.Ui_MainWindow):
    """ Scorplot app class """
    def __init__(self, parent=None):

        # Set up main window
        super(App, self).__init__(parent)
        self.setupUi(self)

        # Instantiate scorbot
        self.scorbot = scorbot.Scorbot()

        # Plot both graphics
        self.initSimulator()

        # Init sliders
        self.initSliders()
        self.updateEndEffector()

        # Connect home button
        self.homeButton.clicked.connect(self.goHome)

        # Connect buttons
        self.goButton.clicked.connect(self.moveEF)
        self.jointsButton.clicked.connect(self.moveJoints)
        self.gripButton.clicked.connect(self.switchGrip)
        self.activateControl.clicked.connect(self.switchControlView)

        # Connect menu items
        self.actionPorts.triggered.connect(self.portSelectionWindow)
        self.actionSave_output.triggered.connect(self.fileToSaveWindow)

        # Init log screen
        # self.initLogScreen()
        self.lineEdit.returnPressed.connect(self.runCommand)

    # Methods
    def initSimulator(self):
        """ Sets the canvas fig on instantiation. """
        pj = self.scorbot.getJointsPos()

        self.simFig = plt.figure(figsize=(6,4))
        self.simCanv = FigureCanvas(self.simFig)
        self.simScene = QtWidgets.QGraphicsScene()
        self.simulatorView.setScene(self.simScene)
        self.simScene.addWidget(self.simCanv)  # Proxy widget

        ax = self.simFig.add_subplot(1,1,1, projection='3d')
        self.ax = ax
        self.ax.set(xlim=[-700, 700], ylim=[-700, 700], zlim=[0, 1000], ylabel='Y', xlabel='X', zlabel='Z', )

        # Draw joints and links
        drawLine(self.ax, [0, 0, 0], pj[0])    # Draw link from base to q1
        for i in range(5):
            if i < 4:
                drawJoint(self.ax, pj[i])          # Draw circle on joint position
            else:
                drawJoint(ax, pj[i], s=60, color="orange", marker="v")
            if i == 0:
                continue
            drawLine(self.ax, pj[i-1], pj[i])  # Draw link from one joint to the next

        # Draw resulting fig
        self.simCanv.draw()

    def cleanControl(self):
        self.conFig = plt.figure(figsize=(6,4))
        self.conCanv = FigureCanvas(self.conFig)
        self.conScene = QtWidgets.QGraphicsScene()
        self.controlView.setScene(self.conScene)
        self.conScene.addWidget(self.conCanv)  # Proxy widget

    def switchControlView(self):
        status = self.activateControl.checkState()
        if status:
            self.initControl()
        else:
            self.cleanControl()

    def initControl(self):
        pj = self.scorbot.getJointsPos()
        self.conFig = plt.figure(figsize=(6,4))
        self.conCanv = FigureCanvas(self.conFig)
        self.conScene = QtWidgets.QGraphicsScene()
        self.controlView.setScene(self.conScene)
        self.conScene.addWidget(self.conCanv)  # Proxy widget

        ax = self.conFig.add_subplot(1,1,1, projection='3d')
        ax.set(xlim=[-700, 700], ylim=[-700, 700], zlim=[0, 1000], ylabel='Y', xlabel='X', zlabel='Z')

        # Draw joints and links
        drawLine(ax, [0, 0, 0], pj[0])    # Draw link from base to q1
        for i in range(5):
            if i < 4:
                drawJoint(ax, pj[i])          # Draw circle on joint position
            else:
                if self.scorbot.grip:
                    color = "green"
                    marker = "^"
                else:
                    color = "orange"
                    marker = "v"
                drawJoint(ax, pj[i], s=60, marker=marker, color=color)
            if i == 0:
                continue
            drawLine(ax, pj[i-1], pj[i])  # Draw link from one joint to the next

        # Draw resulting fig
        self.conCanv.draw()

    def initSliders(self):
        self.joint1ValueLabel.setText("0°")
        self.jointSlider1.valueChanged.connect(self.updateLabelJ1)
        self.joint2ValueLabel.setText("0°")
        self.jointSlider2.valueChanged.connect(self.updateLabelJ2)
        self.joint3ValueLabel.setText("0°")
        self.jointSlider3.valueChanged.connect(self.updateLabelJ3)
        self.joint4ValueLabel.setText("0°")
        self.jointSlider4.valueChanged.connect(self.updateLabelJ4)
        self.joint5ValueLabel.setText("0°")
        self.jointSlider5.valueChanged.connect(self.updateLabelJ5)

    def getSliders(self):
        sliders = []
        sliders.append(self.jointSlider1.value())
        sliders.append(self.jointSlider2.value())
        sliders.append(self.jointSlider3.value())
        sliders.append(self.jointSlider4.value())
        sliders.append(self.jointSlider5.value())
        return np.array(sliders)

    def updateLabelJ1(self):
        value = self.jointSlider1.value()
        self.joint1ValueLabel.setText(f"{value}°")
        # self.scorbot.setJoint(0, value*np.pi/180)
        self.updateEndEffector()

    def updateLabelJ2(self):
        value = self.jointSlider2.value()
        self.joint2ValueLabel.setText(f"{value}°")
        # self.scorbot.setJoint(1, value*np.pi/180)
        self.updateEndEffector()

    def updateLabelJ3(self):
        value = self.jointSlider3.value()
        self.joint3ValueLabel.setText(f"{value}°")
        # self.scorbot.setJoint(2, value*np.pi/180)
        self.updateEndEffector()

    def updateLabelJ4(self):
        value = self.jointSlider4.value()
        self.joint4ValueLabel.setText(f"{value}°")
        # self.scorbot.setJoint(3, value*np.pi/180)
        self.updateEndEffector()

    def updateLabelJ5(self):
        value = self.jointSlider5.value()
        self.joint5ValueLabel.setText(f"{value}°")
        # self.scorbot.setJoint(4, value*np.pi/180)
        self.updateEndEffector()

    def updateSim(self):
        pj = self.scorbot.getJointsPos()
        # ax = self.simFig.add_subplot(1,1,1, projection='3d')

        self.ax.clear()
        self.ax.set(xlim=[-700, 700], ylim=[-700, 700], zlim=[0, 1000], ylabel='Y', xlabel='X', zlabel='Z')

        # Draw joints and links
        drawLine(self.ax, [0, 0, 0], pj[0])    # Draw link from base to q1
        for i in range(5):
            if i < 4:
                drawJoint(self.ax, pj[i])          # Draw circle on joint position
            else:
                if self.scorbot.grip:
                    color = "green"
                    marker = "^"
                else:
                    color = "orange"
                    marker = "v"
                drawJoint(self.ax, pj[i], s=60, marker=marker, color=color)
            if i == 0:
                continue
            drawLine(self.ax, pj[i-1], pj[i])  # Draw link from one joint to the next

        # Draw resulting fig
        self.simCanv.draw()

    def goHome(self):
        self.jointSlider1.setValue(0)
        self.jointSlider2.setValue(0)
        self.jointSlider3.setValue(0)
        self.jointSlider4.setValue(0)
        self.jointSlider5.setValue(0)
        self.moveJoints()

    def updateEndEffector(self):
        # Init new temporal scorbot object
        tempobot = scorbot.Scorbot()

        # Set joint values as in sliders
        tempobot.setJoints( self.getSliders()*np.pi/180 )

        # Get the end effector position
        ef = tempobot.getPos(4).astype(int)

        # Set entry values
        self.xEntry.setValue(ef[0])
        self.yEntry.setValue(ef[1])
        self.zEntry.setValue(ef[2])

    def updateEndEffector2(self):
        # Get endeffector position
        ef = self.scorbot.getPos(4)

        # Set entry values
        self.xEntry.setValue(ef[0])
        self.yEntry.setValue(ef[1])
        self.zEntry.setValue(ef[2])



    def updateSliders(self, thetas=None):
        if thetas is None:
            ik = IK()
            ef = np.array([self.xEntry.value(), self.yEntry.value(), self.zEntry.value()])
            thetas = np.round(ik.solveIK(pxyz=ef)*180/np.pi)

        self.jointSlider1.setValue(round(thetas[0]))
        self.jointSlider2.setValue(round(thetas[1]))
        self.jointSlider3.setValue(round(thetas[2]))
        self.jointSlider4.setValue(round(thetas[3]))

    def portSelectionWindow(self):
        try:
            self.nc
        except:
            self.nc = nucleo.NucleoConnection()

        widget = PortWindow(nc=self.nc)
        widget.exec_()
        self.port = widget.getPort()
        print(f'Openning connection on port: {self.port}')
        self.serial = self.nc.openConnection(self.port)
        self.s2 = scorbot.Scorbot()

    def fileToSaveWindow(self):
        widget = SaveWindow(browserContent=self.textBrowser.toPlainText())
        print(self.textBrowser.toPlainText())
        widget.exec_()

    def isCloseEnough(self, p):
        """
            Returns True if given point is closer than 3 mm of the current end efector position.
            False otherwise.
        """
        cp = self.scorbot.getPos(4).astype(int)

        for i in range(len(cp)):
            if p[i] > (cp[i] - 3) and p[i] < (cp[i] + 3):
                continue
            else:
                return False

        return True

    # Go button
    def moveEF(self):
        ef = np.array([self.xEntry.value(), self.yEntry.value(), self.zEntry.value()])

        if self.isCloseEnough(ef):
            self.textBrowser.append(f'End effector already at that spot')
            return

        print("\nSimulating...\n")
        thetas = IK().solveIK(pxyz=ef)
        print(f"Thetas obtained: {np.round(thetas*np.pi/180, 2)}")
        thetas = np.round(thetas*np.pi/180, 2)

        # Draw robot movement from where it is to target thetas
        self.drawMovement(thetas)

        self.updateSliders(thetas)

        # Update scorbot object's thetas
        # self.scorbot.setJoints(thetas)

    # Joints button
    def moveJoints(self):

        # Get joint values to move to
        print("\nSimulating...\n")
        thetas = self.getSliders()
        thetas = np.round(thetas*np.pi/180, 2)
        print(f"Thetas obtained: {thetas}")

        # Draw robot movement from where it is to target thetas
        self.drawMovement(thetas)

        # Update scorbot object's thetas
        # self.scorbot.setJoints(thetas)

    def drawMovement(self, thetas):
        # Get current position
        cp = self.scorbot.getJoints()

        # Difference from desired to current position on angles
        n = 15
        diffAngles = (thetas - cp)/n

        # Print info
        self.textBrowser.append(f'Thetas: {np.round(thetas/np.pi*180, 1)}')

        for j in range(5):
            if not self.scorbot.q[j].inRange(thetas[j]):
                print(f"{thetas[j]} out of range on joint{j+1}")
                thetas = np.round(thetas/np.pi*180, 1)
                qlim =  np.round(np.array(self.scorbot.q[j].qlim) / np.pi*180)
                self.textBrowser.append(f"{thetas[j]}º out of range on joint{j+1} " +
                    f"which should be between: {qlim}")
                return

        # For each angle division, plot it
        for j in range(1, n):
            self.scorbot.setJoints(cp + diffAngles*j)
            self.simCanv.flush_events()
            self.updateSim()

    def switchGrip(self):
        cs = self.scorbot.grip
        if cs:
            self.scorbot.grip = False
        elif not cs:
            self.scorbot.grip = True
        else:
            print(f'error detecting grip status {cs}')

        self.updateSim()

    def initLogScreen(self):
        # self.textBrowser.setStyleSheet('background-color: #ffffff;')
        # self.textBrowser.setTextBackgroundColor()
        self.textBrowser.clear()

    def runCommand(self):
        command = list(filter(None, self.lineEdit.text().split(" "))) # Splits command string into arguments list
        self.lineEdit.clear()

        if command[0] == "clear":
            self.textBrowser.clear()
            return

        if self.isValidCommand(command):
            try:
                self.sendCommand(command)
            except AttributeError as err:
                self.textBrowser.append(f"Nucleo not found. Go to settings -> port selection and choose " +
                                          "a valid port to connect to lab's scorbot")
                print(err)

        else:
            cs = " ".join(command)
            self.textBrowser.append(" ".join(command))
            self.textBrowser.append("Wrong command. Couldn't be interpreted.  Try again...")

    def isValidCommand(self, command):
        possibleOrders = ["move", "speed", "stop", "open", "close", "get", "save", "load", "home", "start", "status", "clear"]
        if command[0] not in possibleOrders:
            if "".join(list(command[0])[0:3]) == "AT+":
                return True
            return False
        # self.textBrowser.append(command[0])
        # TODO: añadir el resto de comandos posibles
        if command[0] == "move":
            return self.isValidMove(command)
        elif command[0] == "stop":
            return True
        elif command[0] == "speed":
            return self.isValidSpeed(command)
        elif command[0] == "start":
            return True
        elif command[0] == "status":
            return True
        elif command[0] == "open":
            return True
        elif command[0] == "close":
            return True
        elif command[0] == "home":
            return True
        else:
            return False

    def isValidMove(self, command):
        """ Check if arguments are in range int numbers. """
        if not len(command)-1 == 3:
            return False

        for i in command[1:]:
            try:
                j = int(i)
            except Exception as e:
                return False

            if j not in range(-32767, 32767):
                return False

        return True

    def sendCommand(self, command):
        cs = " ".join(command)
        self.textBrowser.append(cs)
        argin = len(command)-1

        print(f'command={command}')

        if "".join(cs[0:3]) == "AT+":
            print(self.serial)
            self.serial.write(f"{cs}\n".encode(encoding='ascii'))

            r=b'init'
            buffer = []
            while len(r) != 0:
                r = self.serial.readline()
                buffer.append(r.decode())

            self.textBrowser.append(" ".join(buffer))

        elif command[0] == "move":
            pV = arginsToArray(command[1:])
            thetas = IK().solveIK(pV)*np.pi/180
            try:
                cmdSent = self.nc.atMove(thetas=thetas)
                self.textBrowser.append(f'Sending command:\n{cmdSent}')
            except:
                self.drawMovement(thetas)
                print(self.scorbot.getPos(4))
                # self.updateEndEffector2()
                grads = np.round(thetas*180/np.pi)
                self.updateSliders(thetas=grads)

        elif command[0] == "status":
            self.nc.atEnco()
            r = self.serial.readline()
            # r = "3418 8009 -6710 -1512 -1501"
            ecs = r.split(" ")
            thetas = []
            for i in ecs:
                thetas.append(int(i))
            thetas = self.nc.encsToRads(thetas)
            self.textBrowser.append(f"Joint angles: {thetas}")

        elif command[0] == "stop":
            self.nc.atStop()
            r = self.serial.readline()
            self.textBrowser.append(r)

        elif command[0] == "speed":
            self.nc.atVelo()
            r = self.serial.readline()
            self.textBrowser.append(r)

        elif command[0] == "load":
            path = command[1]
            argin = len(command)-1
            if argin > 1:
                ptime = command[2]
            else:
                ptime = 3

            with open(path) as f:
                content = f.readlines()

            for line in content:
                self.runCommand(line)
                time.sleep(3)


        elif command[0] == "control":
            thetas = command[1:]
            self.nc.atMove(thetas=thetas)

        elif command[0] == "start":
            self.updateControl()
            self.conCanv.flush_events()

        elif command[0] == "close":
            self.serial.write(f"AT+CLSE\n".encode(encoding='ascii'))

        elif command[0] == "open":
            self.serial.write(f"AT+OPEN\n".encode(encoding='ascii'))

        elif command[0] == "home":
            self.serial.write(f"AT+HOME\n".encode(encoding='ascii'))


    def getEncoders(self):
        self.nc.atEnco()
        r = self.serial.readline().decode().split(" ")[0:5]
        r[-1] = r[-1].split("\r\n")[0]
        encoders = []
        for i in r:
            encoders.append(int(i))
        return encoders

    def updateControl(self):
        encs = self.getEncoders()
        thetas = self.nc.encsToRads(encs)
        self.s2.setJoints(thetas)
        pj = self.s2.getJointsPos()
        ax = self.conFig.add_subplot(1,1,1, projection='3d')
        ax.set(xlim=[-700, 700], ylim=[-700, 700], zlim=[0, 1000], title='Scorbot-ER V', ylabel='Y', xlabel='X', zlabel='Z')

        # Draw joints and links
        drawLine(ax, [0, 0, 0], pj[0])    # Draw link from base to q1
        for i in range(5):
            drawJoint(ax, pj[i])          # Draw circle on joint position
            if i == 0:
                continue
            drawLine(ax, pj[i-1], pj[i])  # Draw link from one joint to the next

        # Draw resulting fig
        self.conCanv.draw()

    def isValidSpeed(self, command):
        """ Check if arguments are in percentage range """
        if command[1] in range(100+1):
            return True
        else:
            return False

    # def addCorpse(self, thetas):
        # tempobot = scorbot.Scorbot()
        # tempobot.setJoints(thetas)
        # pj = tempobot.getJointsPos()

        # ax = self.simFig.add_subplot(1,1,1, projection='3d')
        # ax.set(xlim=[-700, 700], ylim=[-700, 700], zlim=[0, 1000], title='Scorbot-ER V', ylabel='Y', xlabel='X', zlabel='Z')

        # # Draw joints and links
        # drawLine(ax, [0, 0, 0], pj[0])    # Draw link from base to q1
        # for i in range(5):
        #     drawJoint(ax, pj[i])          # Draw circle on joint position
        #     if i == 0:
        #         continue
        #     drawLine(ax, pj[i-1], pj[i])  # Draw link from one joint to the next

        # # Draw resulting fig
        # self.simCanv.draw()

class PortWindow(QDialog, gui.portSelection.Ui_PortWindow):
    def __init__(self, parent=None, nc=nucleo.NucleoConnection()):
        super(PortWindow, self).__init__(parent)
        self.setupUi(self)
        try:
            self.comboBox.addItems(nc.serialPorts())
        except:
            self.comboBox.addItems("No port found")
        self.buttonBox.accepted.connect(self.setPort)

    def setPort(self):
        self.selectedPort = self.comboBox.currentText()

    def getPort(self):
        return self.selectedPort

class SaveWindow(QDialog, gui.fileToSave.Ui_SaveWindow):
    def __init__(self, browserContent, parent=None):
        super(SaveWindow, self).__init__(parent)
        self.browserContent = browserContent
        self.setupUi(self)
        self.buttonBox.accepted.connect(self.saveFile)

    def saveFile(self):
        fileName = self.textEdit.toPlainText()
        if fileName != None :
            with open(fileName, "a") as file:
                for line in self.browserContent:
                    file.write(line)

# Functions
def drawLine(ax, po, pf, samples=5, color='blue'):
    """ Draws a line starting at po and ending at pf. """
    # Make line from po to pf
    x_line = np.linspace(po[0], pf[0], samples)
    y_line = np.linspace(po[1], pf[1], samples)
    z_line = np.linspace(po[2], pf[2], samples)
    ax.plot3D(x_line, y_line, z_line, color)

def drawJoint(ax, p, s=40, color='red', marker=None):
    """ Draws a marker on the given position. """
    ax.scatter3D(p[0], p[1], p[2], s=s, c=color, marker=marker)

def arginsToArray(argins):
    """ Gets cartesian position for x, y, z in cm string and returns them in milimeters int """
    for i in range(len(argins)):
        argins[i] = int(argins[i])
    return argins

if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = App()
    form.show()
    app.exec_()