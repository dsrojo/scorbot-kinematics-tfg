import tkinter as tk
from tkinter import ttk
import scorbot as Scorbot
import numpy as np
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt
from math import pi


class App:
    def __init__(self, master):
        super().__init__()
        self.master = master
        self.master.title("ScorPlot")
        self.master.geometry('1024x768') # Sets window size

        # Create scorbot
        self.scorbot = Scorbot.Scorbot()

        # Tabs
            # tab_control = ttk.Notebook()
            # tab1 = ttk.Frame(tab_control)
            # tab2 = ttk.Frame(tab_control)
            # tab_control.add(tab1, text='First')
            # tab_control.add(tab2, text='Second')
            # tab_control.grid(row=1)

        # Set interface config pieces
        self.createDraw()
        self.setXYZEntries()
        self.setButton()
        self.setScalas()
        self.setTopMenu()

        # Fix column and row sizes
        self.master.rowconfigure(13, minsize=30)
        self.master.columnconfigure(12, minsize=30)
        self.master.columnconfigure(0, minsize=30)
        self.master.columnconfigure(1, minsize=30)

    def qTraceMod(self, var, indx, mode):
        """Assigns values given by scalas modifications to scorbot joint objects."""
        rad = pi/180
        if var == "q1":
            self.scorbot.setJoint(0, self.q1.get()*rad)
        elif var == "q2":
            self.scorbot.setJoint(1, self.q2.get()*rad)
        elif var == "q3":
            self.scorbot.setJoint(2, self.q3.get()*rad)
        elif var == "q4":
            self.scorbot.setJoint(3, self.q4.get()*rad)
        elif var == "q5":
            self.scorbot.setJoint(4, self.q5.get()*rad)

        # Update xyz entry values
        p = self.scorbot.getPos(4)
        self.x_value.set(round(p[0],2))
        self.y_value.set(round(p[1],2))
        self.z_value.set(round(p[2],2))

        self.updateDraw()

        # This is a just for DEBUG print of joint values, DELETE
        a=[]
        for i in range(5):
            a.append(round(self.scorbot.q[i].theta*180/pi,2))
        print(f'thetas: {a}')
        print(np.round(p,3))

    def getJoints(self, event=None):
        """Returns Scorbot joint values for the current XYZ entry values."""
        pV = [self.x_value.get(), self.y_value.get(), self.z_value.get()]
        return self.scorbot.ik(pV)
        #self.results['text'] = f"Joints: {np.round(thetas*180/pi, 2)} deg"

    def start(self):
        """Starts the main loop of the app."""
        self.master.mainloop()

    def drawLine(self, ax, po, pf, samples=1000):
        """Draws a line starting at po and ending at pf."""
        # Make line from po to pf
        x_line = np.linspace(po[0], pf[0], samples)
        y_line = np.linspace(po[1], pf[1], samples)
        z_line = np.linspace(po[2], pf[2], samples)
        ax.plot3D(x_line, y_line, z_line, 'blue')

    def drawJoint(self, ax, p, s=40, c='red'):
        """Draws a circle on the given position."""
        ax.scatter3D(p[0], p[1], p[2], s=s, c=c)

    def updateDraw(self):
        """Updates the Scorbot representation for the current joint values."""
        self.ax.clear()
        self.ax.set(xlim=[-700, 700], ylim=[-700, 700], zlim=[0, 1200], title='Scorbot-ER V', ylabel='Y', xlabel='X', zlabel='Z')
        # Draw joints and links
        pj = self.scorbot.getJointsPos()
        self.drawLine(self.ax, [0, 0, 0], pj[0])    # Draw link from base to q1
        for i in range(5):
            self.drawJoint(self.ax, pj[i])          # Draw circle on joint position
            if i == 0:
                continue
            self.drawLine(self.ax, pj[i-1], pj[i])  # Draw link from one joint to the next
        self.canvas.draw()

    def createDraw(self):
        """Sets the canvas fig on instantiation."""
        # Define plot assets
        fig = plt.figure()
        # ax = fig.add_subplot(111)
        ax = plt.axes(projection="3d")
        ax.set(xlim=[-700, 700], ylim=[-700, 700], zlim=[0, 1200], title='Scorbot-ER V', ylabel='Y', xlabel='X', zlabel='Z')

        # Draw joints and links
        pj = self.scorbot.getJointsPos()
        self.drawLine(ax, [0, 0, 0], pj[0])    # Draw link from base to q1
        for i in range(5):
            self.drawJoint(ax, pj[i])          # Draw circle on joint position
            if i == 0:
                continue
            self.drawLine(ax, pj[i-1], pj[i])  # Draw link from one joint to the next

        # Set canvas config
        self.canvas = FigureCanvasTkAgg(fig, master=self.master)
        self.canvas.draw()
        self.canvas.get_tk_widget().grid(rowspan=10, columnspan=10, column=2, row=3)
        self.ax = ax

    def onClick(self):
        """Updates the canvas fig for the given end-effector position."""
        pV = [self.x_value.get(), self.y_value.get(), self.z_value.get()]
        thetas = self.scorbot.solveIK(pV)
        print(thetas)
        self.results['text'] = f"{thetas}"
        # TODO: check theta values, they seem to be wrong

    def updateQScalas(self, thetas):
        """Updates scalas for the given thetas."""
        deg = 180/pi
        self.q1_scala.set(thetas[0]*deg)
        self.q2_scala.set(thetas[1]*deg)
        self.q3_scala.set(thetas[2]*deg)
        self.q4_scala.set(thetas[3]*deg)
        self.q5_scala.set(thetas[4]*deg)

    def setXYZEntries(self):
        """Sets the position entries on the window."""
        p = self.scorbot.getPos(4)

        # XYZ values
        self.x_value = tk.DoubleVar(self.master, p[0])
        self.y_value = tk.DoubleVar(self.master, p[1])
        self.z_value = tk.DoubleVar(self.master, p[2])

        # X input frame
        tk.Label(self.master, text = "x:").grid(column=4, row=14, sticky="e")
        self.x_entry = tk.Entry(self.master, textvariable=self.x_value, width=8)
        self.x_entry.bind("<Return>", self.getJoints)
        self.x_entry.grid(column=5, row=14, ipadx=0, sticky="w")

        # Y input frame
        tk.Label(self.master, text = "y:").grid(column=4, row=15, sticky="e")
        self.y_entry = tk.Entry(self.master, textvariable=self.y_value, width=8)
        self.y_entry.bind("<Return>", self.getJoints)
        self.y_entry.grid(column=5, row=15, sticky="w")

        # Z input frame
        tk.Label(self.master, text = "z:").grid(column=4, row=16, sticky="e")
        self.z_entry = tk.Entry(self.master, textvariable=self.z_value, width=8)
        self.z_entry.bind("<Return>", self.getJoints)
        self.z_entry.grid(column=5, row=16, sticky="w")

    def setScalas(self):
        """Sets scalas and define qtrace vars."""
        # Define trace vars
        self.q1 = tk.DoubleVar(name="q1")
        self.q2 = tk.DoubleVar(name="q2")
        self.q3 = tk.DoubleVar(name="q3")
        self.q4 = tk.DoubleVar(name="q4")
        self.q5 = tk.DoubleVar(name="q5")

        # Register observer
        self.q1.trace_add('write', self.qTraceMod)
        self.q2.trace_add('write', self.qTraceMod)
        self.q3.trace_add('write', self.qTraceMod)
        self.q4.trace_add('write', self.qTraceMod)
        self.q5.trace_add('write', self.qTraceMod)

        deg = 180/pi
        # Joint sliders
        self.q1_label = tk.Label(self.master, text="Joint 1:")
        self.q1_scala = tk.Scale(self.master, variable=self.q1, from_=self.scorbot.getRange(0)[0]*deg, to=self.scorbot.getRange(0)[1]*deg, orient="horizontal")
        self.q1_label.grid(column=13, row=5)
        self.q1_scala.grid(column=14, row=5)

        self.q2_label = tk.Label(self.master, text="Joint 2:")
        self.q2_scala = tk.Scale(self.master, variable=self.q2, from_=self.scorbot.getRange(1)[0]*deg, to=self.scorbot.getRange(1)[1]*deg, orient="horizontal")
        self.q2_label.grid(column=13, row=6)
        self.q2_scala.grid(column=14, row=6)

        self.q3_label = tk.Label(self.master, text="Joint 3:")
        self.q3_scala = tk.Scale(self.master, variable=self.q3, from_=self.scorbot.getRange(2)[0]*deg, to=self.scorbot.getRange(2)[1]*deg, orient="horizontal")
        self.q3_label.grid(column=13, row=7)
        self.q3_scala.grid(column=14, row=7)

        self.q4_label = tk.Label(self.master, text="Joint 4:")
        self.q4_scala = tk.Scale(self.master, variable=self.q4, from_=self.scorbot.getRange(3)[0]*deg, to=self.scorbot.getRange(3)[1]*deg, orient="horizontal")
        self.q4_label.grid(column=13, row=8)
        self.q4_scala.grid(column=14, row=8)

        self.q5_label = tk.Label(self.master, text="Joint 5:")
        self.q5_scala = tk.Scale(self.master, variable=self.q5, from_=self.scorbot.getRange(4)[0]*deg, to=self.scorbot.getRange(4)[1]*deg, orient="horizontal")
        self.q5_label.grid(column=13, row=9)
        self.q5_scala.grid(column=14, row=9)

    def setButton(self):
        self.button = tk.Button(self.master, text="Draw", command=self.onClick)
        self.button.grid(column=5, columnspan=2, row=18)
        self.results = tk.Label(self.master)
        self.results.grid(column=5, row=19)

    def setTopMenu(self):
        menu_frame = tk.Frame(self.master)
        menu_frame.grid(row=0)

        top_menu = tk.Menu(menu_frame)
        top_menu.add_command(label="Foo1", command=self.foo1)
        top_menu.add_command(label="Foo2", command=self.foo2)
        top_menu.add_command(label="Exit", command=self.master.destroy)

        self.master.config(menu=top_menu)

    # Menu
    def foo1(self):
        #Code to be written
        pass

    def foo2(self):
        #Code to be written
        pass

# Main loop
if __name__ == "__main__":
    root = tk.Tk()
    app = App(root)
    app.start()
