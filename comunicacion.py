import serial
import sys
import glob


def serialPorts():
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        ports = glob.glob('/dev/tty[A-Za-z]*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result


def executeProgram(s):
    cmdList = [
        "AT+OPEN\n",
        "AT+VELO 50\n",
        "AT+CLSE\n",
        "AT+LOAD 0\n",
        "AT+LOAD 1\n",
        "AT+OPEN\n",
        "AT+VELO 25\n",
        "AT+LOAD 2\n",
        "AT+CLSE\n",
        "AT+VELO 45\n",
        "AT+LOAD 1\n",
        "AT+VELO 100\n",
        "AT+LOAD 3\n",
        "AT+VELO 25\n",
        "AT+LOAD 4\n",
        "AT+OPEN\n",
        "AT+VELO 45\n",
        "AT+LOAD 3\n",
        "AT+CLSE\n",
        "AT+LOAD 0\n"
    ]
    for i in range(len(cmdList)):
        print(cmdList[i])
        s.write(cmdList[i].encode())
        encoders = s.readline()
        print(encoders)
        input("Press Enter to continue...")




if __name__=="__main__":

    print("Instantiating NucleoConnection object..")
    ports  = serialPorts()
    if len(ports) > 0:
        print(f"Ports found: {ports}")
    else:
        print("No port was found")

    s = serial.Serial(ports[0], timeout=0.2)
    print(s)



    cmd = f'AT+ENCO\n'
    s.write(cmd.encode(encoding='ascii'))
    print(cmd)

    r=b'init'
    buffer = []
    while len(r) != 0:
        r = s.readline()
        buffer.append(r.decode())

    print(buffer)





    # r = s.readall()
    # print(r.decode())





    s.close()
