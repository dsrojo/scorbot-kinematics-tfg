#!/bin/env/python3

"""Scorbot module to implement every feature."""

import math
import pygame
import numpy as np
from math import pi
from spatialmath import base
from tabulate import tabulate
from pygame import Vector2

class Scorbot():
    """Class that models a Scorbot-ER V manipulator."""

    def __init__(self):
        # deg = pi / 180
        self.max_g = 65/1000         # Maximum aperture of gripper
        self.angles = []

        # Instanciate joints from 1 to 5
        self.q = []
        for qn in [0, 1, 2, 3, 4]:
            self.q.append( Joint(qn) )

        # Init grip
        self.grip = False

        # # zero angles, L shaped pose
            # self.addConfig("qz", np.array([0, 0, 0, 0, 0]))

            # # ready pose, arm up
            # self.addConfig("qr", np.array([0, pi/2, 0, pi/2, 0]))

            # # custom home
            # self.addConfig("qh", np.array([38.15*deg, -30*deg, 45*deg, -63.54*deg, 0*deg]))

            # # straight and horizontal
            # self.addConfig("qs", np.array([0, 0, -pi/2, 0, 0]))

            # # nominal table top picking pose
            # self.addConfig("qn", np.array([0, pi/4, pi, 0, pi/4]))

    def __str__(self):
        dhp = self.getDHParams()
        dhp["alpha"] = np.array(dhp["alpha"])*180/pi
        dhp["qlim"] = np.array(dhp["qlim"])*180/pi

        rows = []
        for i in range(len(dhp["a"])):
            rows.append([f"q{i+1}", dhp["a"][i], dhp["d"][i], dhp["alpha"][i], f'{dhp["qlim"][0][i]} -> {dhp["qlim"][1][i]}'])

        dhparams = tabulate(rows, headers=["a", "d", "alpha", "qlim"], tablefmt="fancy_grid")

        return f"\nScorbot-ER V\n{dhparams}"

    def mdhmod(self, alpha, a, theta, d):
        return np.array([
            [np.cos(theta), -np.sin(theta)*np.cos(alpha), np.sin(theta)*np.sin(alpha), a*np.cos(theta)],
            [np.sin(theta), np.cos(theta)*np.cos(alpha), -np.cos(theta)*np.sin(alpha), a*np.sin(theta)],
            [0, np.sin(alpha), np.cos(alpha), d],
            [0, 0, 0, 1]
        ])

    def getRange(self, qn):
        """Returns the range of the qnth joint"""
        return [self.q[qn].qlim[0], self.q[qn].qlim[1]]

    def addConfig(self, name, thetas):
        # TODO: add method to save thetas config maybe in a map
        pass

    def getDHParams(self):
        """Returns Scorbot Denavit-Hartenberg Parameters as a map of string -> array."""
        deg = pi/180
        return {
                "a": [101.25, 220, 220, 0,       0],
                "d": [334.25,    0,    0, 0, 137.35],
                "alpha": [pi/2, 0, 0, pi/2, 0],
                "qlim": [[-155*deg, -35*deg, -130*deg, -130*deg, -570*deg],
                         [ 155*deg, 130*deg,  130*deg,  130*deg,  570*deg]]
            }

    def getPos(self, qn):
        """Returns qn joint position by T0n calculation."""
        T0n = self.getT0n(qn+1)
        return np.array([T0n[0][3],T0n[1][3],T0n[2][3]])

    def getJoints(self):
        joints = np.array([])
        for i in range(5):
            joints = np.append(joints, self.q[i].theta)
        return joints

    def getJointsPos(self):
        """Returns a numpy array containing every joint position in cartesian coordinates."""
        p = np.array([[0,0,0],[0,0,0], [0,0,0], [0,0,0], [0,0,0]], dtype=object)
        T0n = np.eye(4)
        for i in range(4):
            T0n = np.dot(T0n,self.mdhmod(self.q[i].alpha, self.q[i].a, self.q[i].theta, self.q[i].d))
            if i == 3:
                T0n = np.dot(T0n,self.mdhmod(self.q[i+1].alpha, self.q[i+1].a, self.q[i+1].theta, self.q[i+1].d))
            p[i+1] = [T0n[0][3],T0n[1][3],T0n[2][3]]
        return np.array(p).astype(int)

    def setJoint(self, qn, value):
        self.q[qn].theta = value

    def setJoints(self, thetas):
        # print(f'Thetas: {thetas}')
        for i in range(5):
            self.setJoint(i, thetas[i])

    def getT0n(self, n):
        T0n = np.eye(4)
        for i in range(n):
            T0n = np.dot(T0n, self.mdhmod(self.q[i].alpha, self.q[i].a, self.q[i].theta, self.q[i].d))
        return np.round(T0n,3)

    def solveIK(self, pV):
        return ik.IK().solveIK(pV)

class Joint(Scorbot):
    """Modelates a Scorbot joint as a child of the same name class."""
    def __init__(self, qn):
        self.qn = qn
        self.dhparams = self.getDHParams()
        self.a  = self.dhparams["a"][qn]
        self.d  = self.dhparams["d"][qn]
        self.alpha  = self.dhparams["alpha"][qn]
        self.qlim  = [self.dhparams["qlim"][0][qn], self.dhparams["qlim"][1][qn]]
        self.theta = 0

    @property
    def a(self):
        return self._a

    @a.setter
    def a(self, value):
        if value < 0:
            raise ValueError("link offset should be positive")
        self._a = value

    @property
    def d(self):
        return self._d

    @d.setter
    def d(self, value):
        if value < 0:
            raise ValueError("link length should be positive")
        self._d = value

    @property
    def alpha(self):
        return self._alpha

    @alpha.setter
    def alpha(self, value):
        self._alpha = value

    @property
    def qlim(self):
        return self._qlim

    @qlim.setter
    def qlim(self, minmax):
        self._qlim = minmax

    @property
    def theta(self):
        return self._theta

    @theta.setter
    def theta(self, value):
        if value < self.qlim[0] or value > self.qlim[1]:
            raise ValueError(f"Theta should be within qlim range, not {value} on {self.qn}")
        self._theta = value

    def inRange(self, angle):
        if self.qlim[0] <= angle <= self.qlim[1]:
            return True
        return False

class IK:
    """
    This class provides a safe place to solve the inverse kinematics problem
    """
    def __init__(self):
        self.angles = []
        self.points = []
        # TODO: set a max angle for each joint
        self.maxAngle = 360

    def solveLoop(self, i, endEffector, targetPoint):
        if i < len(self.points) - 2:
            endEffector = self.solveLoop(i+1, endEffector, targetPoint)
        current_point = self.points[i]

        angle = (endEffector-current_point).angle_to(targetPoint-current_point)
        self.angles[i] += min(max(-3, angle), 3)
        self.angles[i] = min(max(180-self.maxAngle, (self.angles[i]+180)%360), 180+self.maxAngle)-180

        return current_point + (endEffector-current_point).rotate(angle)

    def loadPoints(self, q):
        """ Gets rotated Y axis joint positions and returns Vector2 array of points. """
        self.points = list(map(Vector2, []))
        for i in range(4):
            self.points.append(Vector2(q[i][0], q[i][1]))
        return self.points

    def rotateYAxis(self, qpositions, angle):
        n = len(qpositions)
        qnew = np.empty([n,2], dtype=int)
        for i in range(n):
            xnew = qpositions[i][0]*np.cos(angle)
            znew = qpositions[i][1]
            qnew[i] = np.array([xnew, znew], dtype=int)

        return qnew

    def rotatePoint(self, point):
        xnew = np.sqrt(point[0]**2+point[1]**2).astype(int)
        ynew = point[2]
        return [xnew, ynew]

    def solveIK(self, pxyz, iterations=250):
        """ Gets (px, py, px). Returns [q1, q2, q3, q4, q5] degree angles """
        if len(pxyz) != 3:
            print(f"error: given point {pxyz} is not valid")
            return None
        theta1 = np.arctan2(pxyz[1], pxyz[0])*180/np.pi
        pV = self.rotatePoint(pxyz)
        targetPoint = Vector2(pV[0], pV[1])
        q0 = np.array([[101, 334], [321, 334], [541, 334], [541, 196]]) # Initial position of qs

        self.points = self.loadPoints( self.rotateYAxis(q0, theta1) )
        relPoints = []

        for i in range(1, len(self.points)):
            relPoints.append(self.points[i] - self.points[i-1])
            self.angles.append(0)

        print(f"Initial Point: {self.points[-1]}, to reach: {targetPoint}")
        for i in range(iterations):
            self.solveLoop(0, self.points[-1], targetPoint)
            angle = 0
            for j in range(1, len(self.points)):
                angle += self.angles[j-1]
                self.points[j] = self.points[j-1] + relPoints[j-1].rotate(angle)

            if i > 20 and self.isCloseEnough(self.points[-1], pV):
                print(f"Iterations: {i}")
                break


        print(f"Final position: {self.points[-1]}")
        thetas = np.array([theta1])
        for i in range(len(self.angles)):
            thetas = np.append(thetas, self.angles[i])
        thetas = np.append(thetas, 0)

        return thetas

    def isCloseEnough(self, p1, p2):
        p1 = [np.round(float(p1[0]), 2), np.round(float(p1[1]), 2)]
        p2 = [np.round(float(p2[0]), 2), np.round(float(p2[1]), 2)]
        if p1 == p2:
            return True
        return False

    def pointTo3D(self, point, theta1):
        xnew = point[0]*np.cos(theta1)
        ynew = point[0]*np.sin(theta1)
        znew = point[1]
        return [xnew, ynew, znew]

# Execution
if __name__ == '__main__':    # pragma nocover

    s = Scorbot()
    print(s)
    # print(s.solveIK([100, 250, 200]))
