WIP Proyect

# Intro
This porject is currently being developed. It is still a beta phase software. Use with caution.

## Requirements
Some dependencies are needed in order to run the project. The easiest way to get them satisfied is:

On the root directory of the project:

`pip3 install -r requirements.txt`

## Python
If python and/or pip are not installed you can do it by:

### Linux
On apt package manager based distributions:

`apt install python3 python3-pip`

On arch based distributions:

`pacman -S python`

### Windows
You can get the latest stable release from the official [website](https://www.python.org/downloads/windows).
Then, just execute the binary and follow the provided instructions on the wizard.
After python is installed, go ahead and install our requirements:

`pip install -r requirements.txt`